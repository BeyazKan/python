"""
Problem 1
Kullanıcıdan alınan boy ve kilo değerlerine göre beden kitle indeksini
hesaplayın ve şu kurallara göre ekrana şu yazıları yazdırın.

 Beden Kitle İndeksi: Kilo / Boy(m) *  Boy(m)

 BKİ 18.5'un altındaysa -------> Zayıf

 BKİ 18.5 ile 25 arasındaysa ------> Normal

 BKİ 25 ile 30 arasındaysa --------> Fazla Kilolu

 BKİ 30'un üstündeyse -------------> Obez
"""

print("""******************************
Beden Kitle İndeks Programı
******************************
""")

kilo = int(input("Lütfen kilonuzu giriniz :"))
boy = float(input("Lütfen boyunuzu giriniz (m) :"))
bki = kilo / boy ** 2

print("Beden Kitle İndeksiniz : {:6.2f}".format(bki))

if bki < 18.5:
    print("Durumunuz : Zayıf")
elif bki < 25:
    print("Durumunuz : Normal")
elif bki < 30:
    print("Fazla Kilolu")
else:
    print("Durumunuz Aşırı Kilolu")