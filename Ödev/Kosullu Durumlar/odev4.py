"""
Problem 4
Şimdi de geometrik şekil hesaplama işlemi yapalım. İlk olarak kullanıcıdan üçgenin mi
dörtgenin mi tipini bulmak istediğini sorun.

Eğer kullanıcı "Dörtgen" cevabını verirse , 4 tane kenar isteyip bu dörtgenin kare mi ,
dikdörtgen mi yoksa sıradan bir dörtgen mi olduğunu bulmaya çalışın.

Eğer kullanıcı "Üçgen" cevabını verirse , 3 tane kenar isteyip bu üçgenin ikizkenar mı ,
eşkenar mı yoksa sıradan bir üçgen mi olduğunu bulmaya çalışın. Eğer verilen kenarlar bir
üçgen belirtmiyorsa, ekrana "Üçgen belirtmiyor" şeklinde bir yazı yazın.o

Üçgen belirtme şartını bilmiyorsunuz internetten bakabilirsiniz.

Ayrıca , bu problemde mutlak değer bulmaya ihtiyacınız olacak. Bunun için, Pythonda hazır
bir fonksiyon olan abs() fonksiyonunu kullanabilirsiniz. Kullanımı şu şekildedir ;

In [1]:
abs(-4)
Out[1]:
4
In [2]:
abs(5)
Out[2]:
5
Not: Bu problem sizin algoritma kurma becerinizi bir hayli geliştirecektir.
"""

print("""
Geometrik Hesaplama
*******************
Seçenekler;

1. Dikdörtgen
2. Üçgen
""")

secim = int(input("Lütfen yukardaki seçeneklerden birini seçin:"))
if secim == 1:
    a = int(input("İlk kenar uzunluğu:"))
    b = int(input("İkinci kenar uzunluğu"))
    c = int(input("Üçüncü kenar uzunluğu"))
    d = int(input("Dördüncü Kenar Uzunluğu"))
    if (a == b and a == c and a == d):
        print("Kare")
    elif (a == c and b == d):
        print("Dikdörtgen")
    else:
        print("Dörtgen")

else:
    a = int(input("İlk Kenar Uzunluğu :"))
    b = int(input("İkinci Kenar Uzunluğu :"))
    c = int(input("Üçüncü Kenar Uzunluğu :"))

    if (abs(a+b) > c and abs(a+c) > b and abs(b+c)> a):
        if(a == b and a == c):
            print("Eşkenar Üçgen")
        elif ((a == b and a != c) or (a == c and a != b) or (b == c and b != a)):
            print("İkizkenar Üçgen....")
        else:
            print("Çeşitkenar Üçgen...")
    else:
        print("Üçgen belirtmiyor...")