"""
Problem 2
Kullanıcıdan 3 tane sayı alın ve en büyük sayıyı ekrana yazdırın.
"""

print("""
***************************************
En büyük sayıyı yazdırma programı
***************************************
""")

a = int(input("Birinci Sayıyı Giriniz :"))
b = int(input("İkinci Sayıyı Giriniz :"))
c = int(input("Üçüncü Sayıyı Giriniz :"))

if a > b and a > c:
    print("En Büyük Sayı Değeri :", a)
elif b > a and b > c:
    print("En Büyük Sayı Değeri :", b)
else:
    print("En Büyük Sayı Değeri :", c)