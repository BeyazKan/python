"""
Problem 3
Bir aracın kilometrede ne kadar yaktığı ve kaç kilometre yol
yaptığı bilgilerini alın ve sürücünü toplam ne kadar ödemesini gerektiğini hesaplayın.
"""

print("Araç Yakıt Masrafının Hesaplanması")

yakit_ucreti = float(input("Aracınızın 1 km'de yaktığı ücreti giriniz:"))
km = float(input("Kaç kilometre yol alacasınız:"))

print("Aracınızın {} km'lik yol için gerekli yakıt ücreti {} TL dir".format(km, yakit_ucreti * km))