"""
Problem 6
Kullanıcıdan bir dik üçgenin dik olan iki kenarını(a,b) alın ve hipotenüs uzunluğunu bulmaya çalışın.

Hipotenüs Formülü: a ** 2 + b ** 2 = c^2
"""

print("------Hipotenüs uzunluğunu bulmak------")
a = int(input("Lütfen 1.kenarın uzunluğunu yazınız:"))
b = int(input("Lütfen 2.kenarın uzunluğunu yazınız:"))

uzunluk = (a ** 2 + b ** 2) ** 0.5

print("Hipotenüsün uzunluğu:", uzunluk)