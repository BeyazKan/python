"""
Problem 2
Kullanıcıdan aldığınız boy ve kilo değerlerine göre kullanıcının beden kitle indeksini bulun.

Beden Kitle İndeksi : Kilo / Boy(m) Boy(m)
"""
print("Vücut Kitle İndeksini Bulma Programı")

kilo = int(input("Lütfen kilonuzu giriniz :"))
boy = float(input("Lüfen boyunuzu giriniz :"))

print("Beden Kitle İndeksi:", kilo / (boy ** 2))