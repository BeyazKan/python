"""
Problem 5
Kullanıcıdan iki tane sayı isteyin ve bu sayıların değerlerini birbirleriyle değiştirin.
"""

a = int(input("Lütfen bir sayı giriniz :"))
b = int(input("Lütfen bir sayı daha giriniz:"))

print("a = {}, b = {}".format(a, b))

a, b = b, a;

print("a = {}, b = {}".format(a, b))