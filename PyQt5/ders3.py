import sys

from PyQt5 import QtWidgets

def Pencere():
    app = QtWidgets.QApplication(sys.argv)

    pencere = QtWidgets.QWidget()
    pencere.setWindowTitle("PyQt5 Ders 3")
    pencere.setGeometry(100,100,500,500)

    button = QtWidgets.QPushButton(pencere)
    button.setText("Burası bir buttondur.")
    button.move(10,30)

    label = QtWidgets.QLabel(pencere)
    label.setText('Merhaba Dünya')
    label.move(20, 10)


    pencere.show()

    sys.exit(app.exec_())

Pencere()