import sys

from PyQt5 import QtWidgets, QtGui

def Pencere():
    app = QtWidgets.QApplication(sys.argv)

    pencere = QtWidgets.QWidget()
    pencere.setWindowTitle("PyQt5 Ders 1")
    pencere.setGeometry(100,100,500,500)
    label1 = QtWidgets.QLabel(pencere)
    label1.setText("Burası bir yazıdır.")
    label1.move(200,30)

    label2 = QtWidgets.QLabel(pencere)
    label2.setPixmap(QtGui.QPixmap("mortal_kombat_icon.png"))
    label2.move(200,50)

    pencere.show()

    sys.exit(app.exec_())

Pencere()