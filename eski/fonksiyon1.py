def topla(*args):
    sonuc = 0
    for i in args:
        sonuc = sonuc + i
    return sonuc

#print(topla(2, 5))
#print(topla(1,2,3,4,5))
#print(topla(18))

def toplab(baslangic, *a):
    sonuc = baslangic
    print("baslangic:" + str(baslangic))
    for i in a:
        sonuc = sonuc + i
    return sonuc

#print(toplab(2,3,4,5))

def fonksiyon(**a):
    for i in a:
        print(str(i) + str(a[i]))

fonksiyon(a = 2, b = 3, c = 4)