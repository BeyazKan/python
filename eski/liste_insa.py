l = []

for x in range(1, 11):
    l.append(x**2)

print(l)
print(x)

squares = list(map(lambda y: y**2, range(1, 11)))

print(squares)

def f(x):
    return x+5

l2 = [2, 8, 3]

print(list(map(lambda x: x+5, l2)))

l3 = [z**2 for z in range(10)]
print(l3)

l4 = [(x, y, z) for x in [1, 2, 3] for y, z in [(1,2), (2,3), (3,4)] if x != y]
print(l4)