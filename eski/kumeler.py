l = [1, 2, 3, 1, 2, 1, 3] # Liste
t = (1, 2, 3) # Kayit
k = {1, 2, 3, 1, 2, 1, 3} # Kümeler set

print(l)
print(t)
print(k)

k2 = set(l)
k3 = set([1, 2,3, 1, 32, 3])
print(k2)
print(k3)

k4 = set('bilgisayarkavramlari')
k5 = set('sadievrenseker')

print(k4)
print(k5)

print(k4|k5) # set union, birleşim operatörü
print(k4-k5) # küme farkı
print(k4&k5) # küme kesisim
print(k4^k5) # exclusive or, özel veya, iki yönlü küme farkının birleşimi