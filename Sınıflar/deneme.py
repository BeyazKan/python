

class Kisi():
    Ad = ""
    Soyad = ""
    Yas = 0
    Cinsiyet = ""

    def __init__(self, Ad = "[Bilgi Yok]", Soyad = "[Bilgi Yok]", Yas = 0, Cinsiyet = "Bilgi Yok"):
        self.Ad = Ad
        self.Soyad = Soyad
        self.Yas = Yas
        self.Cinsiyet = Cinsiyet

    def BilgileriGoster(self):
        print("""
            Kişi objesinin özellikleri
            
            İsim : {}
            Soyisim: {}
            Yaş: {}
            Cinsiyet: {}
        """.format(self.Ad, self.Soyad, self.Yas, self.Cinsiyet))


kisi1 = Kisi("Mustafa", "Oğuz", 29, "Erkek")
kisi2 = Kisi("Talha", "Sancar", 31, "Erkek")

kisi1.BilgileriGoster()
kisi2.BilgileriGoster()
