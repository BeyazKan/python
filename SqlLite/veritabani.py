import sqlite3

connect = sqlite3.connect("veritabani.db")
cursor = connect.cursor()


def tablo_olustur():
    cursor.execute("CREATE TABLE IF NOT EXISTS kitaplik (isim TEXT, yazar TEXT, yayinevi TEXT, Sayfa_sayisi INT)")
    connect.commit()

def veri_ekle(veriler = list()):
    cursor.execute("INSERT INTO kitaplik VALUES (?, ?, ?, ?)", (veriler[0], veriler[1], veriler[2], veriler[3]))
    connect.commit()

def verileri_al():
    cursor.execute("SELECT * FROM kitaplik")
    liste = cursor.fetchall()
    print("Kitaplık tablosunun bilgileri:")
    for i in liste:
        print(i)
def verileri_al2():
    cursor.execute("SELECT isim, yazar FROM kitaplik")
    liste = cursor.fetchall()
    print("Kitaplık tablosunun bilgileri:")
    for i in liste:
        print(i)
def verileri_al3(yayinevi):
    cursor.execute("SELECT * FROM kitaplik WHERE yayinevi = ?",(yayinevi,))
    liste = cursor.fetchall()
    print("Kitaplık tablosunun bilgileri:")
    for i in liste:
        print(i)

def verileri_guncelle(eski, yeni):
    cursor.execute("UPDATE kitaplik SET yayinevi = ? WHERE yayinevi = ?", (yeni, eski))
    connect.commit()

def verileri_sil(yazar):
    cursor.execute("DELETE FROM kitaplik WHERE yazar = ?", (yazar,))
    connect.commit()

"""
print("Kitap ekle")
print("-------------------------")
veriler = list()
veriler.append(input("Kitap Adı: "))
veriler.append(input("Yazar : "))
veriler.append(input("Yayınevi : "))
veriler.append(input("Sayfa Sayısı : "))

veri_ekle(veriler)"""
verileri_al()
verileri_sil("Pardus")


connect.close()
