print("""*******************************
Hesap Makinesi Programı

İşlemler;
1.Toplama
2.Çıkarma
3.Çarpma
4.Bölme

*******************************
""")

a = int(input("Birinci Sayıyı Giriniz:"))
b = int(input("İkinci Sayıyı Giriniz:"))

islem = input("İşlem giriniz:")

if islem == "1":
    print("{} ile {}'in toplamı {} dır".format(a, b, a + b))
elif islem == "2":
    print("{} ile {}'in farkı {} dır".format(a, b, a - b))
elif islem == "3":
    print("{} ile {}'in çarpımı {} dır".format(a, b, a * b))
elif islem == "4":
    print("{} ile {}'in bölümü {} dır".format(a, b, a / b))
else:
    print("Geçersiz işlem")